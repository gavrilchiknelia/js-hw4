'use strict';
function checkString(arg, arg2) {
    while (arg === '' || !isNaN(arg)){
        arg = prompt(`Enter your ${arg2} name: `, '');
    }
    return  arg;
}

function CreateNewUser() {
    let obj = {
        firstName: prompt('Введите имя', ''),
        lastName: prompt('Введите фамилию', ''),
        setFirstName: function(value) {
            Object.defineProperty(this, 'firstName', {
                writable: true
            });
            this.firstName = value;
            Object.defineProperty(this, 'firstName', {
                writable: false
            });
        },
        setLastName: function(value) {
            Object.defineProperty(this, 'lastName', {
                writable: true
            });
            this.lastName = value;
            Object.defineProperty(this, 'lastName', {
                writable: false
            });
        },
        getLogin : function () {
            return this.firstName.charAt(0) + this.lastName;
        }
    };

    Object.defineProperties(obj, {
        'firstName': {
            writable: false
        },
        'lastName': {
            writable: false
        }
    });

    return obj;

}

let newUser = new CreateNewUser();

console.log(newUser);
console.log(newUser.getLogin());
newUser.setFirstName('Lili');
console.log(newUser);
newUser.firstName = 'Bob';
console.log(newUser);